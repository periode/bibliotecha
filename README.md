# bibliotecha

	
Bibliotecha is a framework to facilitate the local distribution of digital publications within a small community. It relies on a microcomputer running open-source software to serve books over a local wifi hotspot. Using the browser to connect to the library one can retrieve or donate texts. Bibliotecha proposes an alternative model of distribution of digital texts that allows specific communities to form and share their own collections.

In practice, it is a self-hosted, portable web library. It is generally installed on a raspberry pi, and creates a local wifi network with a captive portal, to let anyone access digital books, essays, zines and comics!

[bibliotecha.info](https://web.archive.org/web/20220209164103/http://bibliotecha.info/)

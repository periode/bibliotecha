FROM debian:bullseye

RUN echo 'root:root' | chpasswd
RUN printf '#!/bin/sh\nexit 0' > /usr/sbin/policy-rc.d
RUN apt-get update
RUN apt-get install -y systemd systemd-sysv dbus dbus-user-session
RUN printf "systemctl start systemd-logind" >> /etc/profile
# RUN /sbin/init

COPY "./bibliotecha.sh" .
RUN bash ./bibliotecha.sh
# ENTRYPOINT ["bash", "./bibliotecha.sh"]
